package com.example.anmchong.cleanscan;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Color;
import android.os.Build;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Arrays;

public class MainActivity extends AppCompatActivity {

    protected Button mStartUpdatesButton;
    protected Button mStopUpdatesButton;

    Button DegValButton;
    Button RadValButton;

    TextView TV1;
    TextView TV2;
    TextView TV3;
    TextView TV4;
    EditText DegVal;
    EditText RadVal;
    EditText OffsetVal;
    EditText YourEmailText;

    EditText BecPText;
    EditText BecAText;
    EditText BecBText;
    EditText BecCText;

    RelativeLayout ColorLayout;

    private BluetoothAdapter mBluetoothAdapter;
    private boolean mScanning;
    private Handler mHandler;
    String StringOfDataFinal = "";
    String StringOfDataIB = "";
    private static final int REQUEST_ENABLE_BT = 1;

    int maxReadings = 4;

    private static final long SCAN_PERIOD = 100000;

    private static final int PERMISSION_REQUEST_COARSE_LOCATION = 1;


// Beacon names here, this app will only take data on beacons in this list....
    String MeasuredIbeaconxW1I = "xW1I"; // the bus beacon 200ms 6dBm
//    String MeasuredIbeaconUos6 = "Uos6"; // Beacon C 200ms 6dBm
//    String MeasuredIbeacond2Rq = "d2Rq"; // Beacon B 200ms 6dBm
//    String MeasuredIbeaconpYEM = "pYEM"; // Beacon A 200ms 6dBm

    String MeasuredIbeaconUos6 = "33tM"; // Beacon C 200ms 6dBm
    String MeasuredIbeacond2Rq = "XHzQ"; // Beacon B 200ms 6dBm
    String MeasuredIbeaconpYEM = "pWEE"; // Beacon A 200ms 6dBm

    String MeasuredBUS928 = "z1dy";
    String MeasuredBUS921 = "mnZr";



    int[][] BeaconVal = new int[12][256];
    int[] AverageRSSI = new int [12];
    int[] ReadingIndex = new int [12];
    int BeaconCount = 4; // number of beacons we want to read

//    int[0] StorexW1I = new int[20];
//    int[1] StoreUos6 = new int[20];
//    int[2] Stored2Rq = new int[20];
//    int[3] StorepYEM = new int[20];



    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mHandler = new Handler();

        mStartUpdatesButton = (Button) findViewById(R.id.start_updates_button);
        mStopUpdatesButton = (Button) findViewById(R.id.stop_updates_button);
        DegValButton = (Button) findViewById(R.id.DegUpButton);
        RadValButton = (Button) findViewById(R.id.RadUpButton);
        TV1 = (TextView) findViewById(R.id.textView);
        TV2 = (TextView) findViewById(R.id.textView2);
        TV3 = (TextView) findViewById(R.id.textView3);
        TV4 = (TextView) findViewById(R.id.textView4);
        DegVal = (EditText) findViewById(R.id.DegVal);
        RadVal = (EditText) findViewById(R.id.radiusVal);
        OffsetVal = (EditText) findViewById(R.id.Offsetval);
        BecPText = (EditText) findViewById(R.id.BecP);
        BecPText.setText(MeasuredIbeaconxW1I);
        BecAText = (EditText) findViewById(R.id.BecA);
        BecAText.setText(MeasuredIbeaconpYEM);
        BecBText = (EditText) findViewById(R.id.BecB);
        BecBText.setText(MeasuredIbeacond2Rq);
        BecCText = (EditText) findViewById(R.id.BecC);
        BecCText.setText(MeasuredIbeaconUos6);
        YourEmailText = (EditText) findViewById(R.id.YourEmail);
        RadVal.setText("0.5");
        DegVal.setText("0");
        OffsetVal.setText("0");

        ColorLayout = (RelativeLayout) findViewById(R.id.activity_main);

        Arrays.fill(BeaconVal[0],1);
        Arrays.fill(BeaconVal[1],1);
        Arrays.fill(BeaconVal[2],1);
        Arrays.fill(BeaconVal[3],1);

        // Use this check to determine whether BLE is supported on the device.  Then you can
        // selectively disable BLE-related features.
        if (!getPackageManager().hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Toast.makeText(this, "BLE not supported", Toast.LENGTH_SHORT).show();
            finish();
        }

        // Initializes a Bluetooth adapter.  For API level 18 and above, get a reference to
        // BluetoothAdapter through BluetoothManager.
        final BluetoothManager bluetoothManager =
                (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
        mBluetoothAdapter = bluetoothManager.getAdapter();
//        mBluetoothAdapter.enable();

        // Checks if Bluetooth is supported on the device.
        if (mBluetoothAdapter == null) {
            Toast.makeText(this, "Bluetooth not supported", Toast.LENGTH_SHORT).show();
            finish();
            return;
        }

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (this.checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

                final AlertDialog.Builder builder = new AlertDialog.Builder(this);

                builder.setTitle("This app needs location access");
                builder.setMessage("Please grant location access so this app can detect beacons.");
                builder.setPositiveButton(android.R.string.ok, null);

                builder.setOnDismissListener(new DialogInterface.OnDismissListener() {


                    @Override

                    public void onDismiss(DialogInterface dialog) {
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            requestPermissions(new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, PERMISSION_REQUEST_COARSE_LOCATION);
                        }

                    }


                });

                builder.show();
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[],
                                           int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_COARSE_LOCATION: {
                if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Log.d(TAG, "coarse location permission granted");
                } else {
                    final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                    builder.setTitle("Functionality limited");
                    builder.setMessage("Since location access has not been granted, this app will not be able to discover beacons when in the background.");
                    builder.setPositiveButton(android.R.string.ok, null);
                    builder.setOnDismissListener(new DialogInterface.OnDismissListener() {
                        @Override
                        public void onDismiss(DialogInterface dialog) {
                        }
                    });
                    builder.show();
                }
                return;
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

        // Ensures Bluetooth is enabled on the device.  If Bluetooth is not currently enabled,
        // fire an intent to display a dialog asking the user to grant permission to enable it.

        // Initializes list view adapter.
        //scanLeDevice(true);
    }


        @Override
        protected void onActivityResult(int requestCode, int resultCode, Intent data) {
            // User chose not to enable Bluetooth.
            if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
                finish();
                return;
            }
            super.onActivityResult(requestCode, resultCode, data);
        }

    @Override
    protected void onPause() {
        super.onPause();
//        scanLeDevice(false);
    }

    private void scanLeDevice(final boolean enable) {
        if (enable) {
            mScanning = true;
            mBluetoothAdapter.startLeScan(mLeScanCallback);
        } else {
            mScanning = false;
            mBluetoothAdapter.stopLeScan(mLeScanCallback);
        }
        invalidateOptionsMenu();
    }

    private BluetoothAdapter.LeScanCallback mLeScanCallback =
            new BluetoothAdapter.LeScanCallback() {

                @Override
                public void onLeScan(final BluetoothDevice device, int rssi, byte[] scanRecord) {
                    //device.EXTRA_RSSI = Integer.toString(rssi);
                    String mys = new String(scanRecord);
                    String MeasuredEddyUID = "xW1I";    //Eddystone device name we want to read
                    String MeasuredIbeacon = "xW1I";    //Ibeacon device name we want to read

                    String FoundMeasureUID = "";        // initializing strings for future comparisons
                    String FoundMeasureURL = "";
                    String FoundMeasureIB = "";
                        //Toast.makeText(getApplicationContext(), "  RSSI: " + rssi + "dBm", Toast.LENGTH_SHORT).show();
                        // The above code will notify the user when a beacon is being detected along with the device RSSI
                        //StringOfData += device.getName() + "\n"+"RSSI:"+Integer.toString(rssi) +"\n"+"Bytes:"+scanRecord+"\n"+mys+"\n\n";
                        StringBuilder sb = new StringBuilder();
                        for (byte b : scanRecord) {
                            sb.append(String.format("%02X ", b));
                        }
                        FoundMeasureUID += Character.toString((char) scanRecord[45])+Character.toString((char) scanRecord[46])
                                +Character.toString((char) scanRecord[47])+Character.toString((char) scanRecord[48]);
                        FoundMeasureURL += Character.toString((char) scanRecord[47])+Character.toString((char) scanRecord[48])
                                +Character.toString((char) scanRecord[49])+Character.toString((char) scanRecord[50]);
                        FoundMeasureIB += Character.toString((char) scanRecord[46])+Character.toString((char) scanRecord[47])
                                +Character.toString((char) scanRecord[48])+Character.toString((char) scanRecord[49]);

                    if (FoundMeasureIB.compareTo(MeasuredIbeaconxW1I) == 0) {
                        StringOfDataIB +="" + rssi;
                        TV1.setText("Prox:   " + Integer.toString(rssi));
                        //ColorLayout.setBackgroundColor(Color.GREEN);
                        BeaconVal [0][ReadingIndex[0]] = rssi;
                        ReadingIndex[0]++;
                        if (ReadingIndex[0]>maxReadings)
                            ReadingIndex[0] = 0;
                    }
                    if (FoundMeasureIB.compareTo(MeasuredIbeaconUos6) == 0) {
                        StringOfDataIB +="" + rssi;
                        TV2.setText("BecC:   " + Integer.toString(rssi));
                        BeaconVal [1][ReadingIndex[1]] = rssi;
                        ReadingIndex[1]++;
                        if (ReadingIndex[1]>maxReadings)
                            ReadingIndex[1] = 0;
                    }
                    if (FoundMeasureIB.compareTo(MeasuredIbeacond2Rq) == 0) {
                        StringOfDataIB +="" + rssi;
                        TV3.setText("BecB:   " + Integer.toString(rssi));
                        BeaconVal [2][ReadingIndex[2]] = rssi;
                        ReadingIndex[2]++;
                        if (ReadingIndex[2]>maxReadings)
                            ReadingIndex[2] = 0;
                    }
                    if (FoundMeasureIB.compareTo(MeasuredIbeaconpYEM) == 0) {
                        StringOfDataIB +="" + rssi;
                        TV4.setText("BecA:   " + Integer.toString(rssi));
                        BeaconVal [3][ReadingIndex[3]] = rssi;
                        ReadingIndex[3]++;
                        if (ReadingIndex[3]>maxReadings)
                            ReadingIndex[3] = 0;
                    }
                    if (FoundMeasureIB.compareTo(MeasuredBUS928) == 0) {
                        //StringOfDataIB +="" + rssi;
                        TV1.setText("BUS: 928" + Integer.toString(rssi));
                        ColorLayout.setBackgroundColor(Color.GREEN);
                    }
                    if (FoundMeasureIB.compareTo(MeasuredBUS921) == 0) {
                        //StringOfDataIB +="" + rssi;
                        TV1.setText("BUS: 921" + Integer.toString(rssi));
                        ColorLayout.setBackgroundColor(Color.RED);
                    }

                    if ((FoundMeasureUID.compareTo(MeasuredEddyUID) == 0) |
                            (FoundMeasureIB.compareTo(MeasuredIbeaconxW1I) == 0) |
                            (FoundMeasureIB.compareTo(MeasuredIbeaconUos6) == 0) |
                            (FoundMeasureIB.compareTo(MeasuredIbeacond2Rq) == 0) |
                            (FoundMeasureIB.compareTo(MeasuredIbeaconpYEM) == 0) |
                            (FoundMeasureURL.compareTo(MeasuredEddyUID) == 0) |
                            (FoundMeasureIB.compareTo(MeasuredIbeacon) == 0) )
                    {
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                            }
                        });
                    }
                }
            };

    public void startUpdatesButtonHandler(View view) {
        scanLeDevice(true);
    }

    public void RadUpClickListener(View view) {
        float Radvalcurrent = new Float(RadVal.getText().toString()).floatValue();
        Radvalcurrent += 0.5;
        if (Radvalcurrent > 10){
            Radvalcurrent = (float)0.5;
        }
        RadVal.setText(Float.toString(Radvalcurrent));
    }

    public void DegUpClickListener(View view) {
        float Degvalcurrent = new Float(DegVal.getText().toString()).floatValue();
        Degvalcurrent += 30;
        if (Degvalcurrent >= 360){
            Degvalcurrent = 0;
        }
        DegVal.setText(Float.toString(Degvalcurrent));
    }

    public void OffsetClickListener(View view) {
        float OffsetValcurrent = new Float(OffsetVal.getText().toString()).floatValue();
        OffsetValcurrent += 90;
        if (OffsetValcurrent >= 360){
            OffsetValcurrent = 0;
        }
        OffsetVal.setText(Float.toString(OffsetValcurrent));
    }

    public void SaveButtonHandler(View view) {
        Intent i = new Intent(Intent.ACTION_SEND);
        i.setType("message/rfc822");
        i.putExtra(Intent.EXTRA_EMAIL, new String[] {YourEmailText.getText().toString()});
        i.putExtra(Intent.EXTRA_SUBJECT, "FingerPrinting Table data");
        i.putExtra(Intent.EXTRA_TEXT, StringOfDataFinal);
        StringOfDataFinal = "";
        try {
            startActivity(Intent.createChooser(i, "Send mail..."));
        } catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(MainActivity.this, "There are no email clients installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public void stopUpdatesButtonHandler(View view) {
        scanLeDevice(false);
        ColorLayout.setBackgroundColor(Color.WHITE);
        int avgindex = 0;

        int beaconindex = 0;
        for(;beaconindex<BeaconCount;) {
            for (AverageRSSI[beaconindex] = 0; (BeaconVal[beaconindex][avgindex] != 1) && (avgindex < 20); ) {
                AverageRSSI[beaconindex] += BeaconVal[beaconindex][avgindex];
                avgindex++;
            }
            if (avgindex == 0) {// there were no read values
                if (beaconindex == 0)
                    TV1.setText("avgP:" + "none");
                if (beaconindex == 1)
                    TV2.setText("avgC:" + "none");
                if (beaconindex == 2)
                    TV3.setText("avgB:" + "none");
                if (beaconindex == 3)
                    TV4.setText("avgA:" + "none");
                AverageRSSI[beaconindex] = 0;
            } else {
                AverageRSSI[beaconindex] = AverageRSSI[beaconindex] / avgindex;
                if (beaconindex == 0)
                    TV1.setText("avgP:" + AverageRSSI[beaconindex]);
                if (beaconindex == 1)
                    TV2.setText("avgC:" + AverageRSSI[beaconindex]);
                if (beaconindex == 2)
                    TV3.setText("avgB:" + AverageRSSI[beaconindex]);
                if (beaconindex == 3)
                    TV4.setText("avgA:" + AverageRSSI[beaconindex]);
            }
            beaconindex++;
            avgindex = 0;
        }

        StringOfDataFinal +=""+AverageRSSI[3]+","+AverageRSSI[2]+","+ AverageRSSI[1]+","
                +DegVal.getText().toString()+","+RadVal.getText().toString()+
                ","+OffsetVal.getText().toString()+"\n";

        Arrays.fill(BeaconVal[0],1);
        Arrays.fill(BeaconVal[1],1);
        Arrays.fill(BeaconVal[2],1);
        Arrays.fill(BeaconVal[3],1);
        Arrays.fill(ReadingIndex,0);
    }


}